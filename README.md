# Learning NextJS

10.10.2020 – [Tutorial Link](https://www.youtube.com/watch?v=tt3PUvhOVzo)

## First steps

- turn your app into typescript
  - TS compilation is created automatically by nextjs
  - rename \_app.js and index.js to .TSX
  - rename /api/.js into .TS
- install typescript and code autocompleter
  - npm install typescript @types/node @types/react --save-dev
- npm run dev
  - takes care of compiling to TS, creating a tsconfig, hot reloading...

## File structure

- `/public`: holds static files that are accessible in the root domain
- `/pages`: source code to create the pages
- `/pages/_document.tsx`: root _html_ that next use to build every page (executed on server only)
- `/pages/_app.tsx`: root _component_ that next use to build every page (executed on client & server)

## Styling

###### CSS-in-JS

- Just write the css under `<style jsx>`.
- CSS is scoped to a particular component.
- Use `global` tag to apply styles wherever a component is included.
- Use variables with `${variable}`.

```
<style jsx global>
  {`
    h1 {
      color: ${variable};
      font-size: 2em;
    }
  `}
</style>
```

###### Global Styles

- Import `global.css` into `_app.tsx`

###### CSS Modules

- Name your css files `name.module.css`
- Your css needs to be specific w/ at least one class/id selector.
- `import styles from "../styles/name.module.css`
- Use is with `className = {styles.container}`

###### SASS Modules

- Next has native support for SASS. Just `npm i sass`
- Rename `name.module.css` -> `name.module.scss`
- Now you can nest styles, use variables, css functions...

## API

- Makes requests to the server
- If file is `name.js` it can be fount at `localhost:3000/api/name`

###### REQUEST

- `req.method === 'POST'`
- `req.body`
- `req.query`

###### RESPONSE

- `res.statusCode = 200;`
- `res.setHeader("Content-Type", "application/json");`
- `res.json({ name: "John Doe" });`
- `res.send()`
- `res.end()`

## Routing

- Page names reflect their URL
- Add folders to add depth into URL eg. `/projects/apple`
- Use `[id]` to add a variable on the file or folder eg. `id/[id]` -> `/id/12345`
- Use `[...name]` to _catch-all_ routes eg. `/name/1/2/3/4/5/6/7`
- Use `[[...name]]` to _catch-all_, including base URL eg. same file works for `/name` and `/name/1/2/3`

## Static Pages

###### getStaticProps

- Executes on the Server at **build time**, not run time
- Can access all Nodejs stuff: database, filesystem, ...

```
export const getStaticProps: GetStaticProps = async (context) => {
  console.log(context.params.id);
  return {
    // Map props to component
    props: {
      favoriteNumber: 4,
    },
    // Update getStaticProps every 10 seconds (production only)
    revalidate: 10,
  };
};
```

###### getStaticPaths

- Compulsory when using dynamic [slugs], so we need to **pre-populate** the URL
- We use this to map path to which page that needs to getStaticProps
- Process:
  - BUILD START
    - localhost:3000/dynamic/hello -> take the output -> store in the disk
    - localhost:3000/dynamic/world -> take the output -> store in the disk
  - If fallback = false
    - DONE -> Any other route will be discarted as 404
  - If fallback = true
    - It will accept any URL, but will only create static pages for the ones in the list
    - eg. localhost:3000/dynamic/_jewo73kqiejwo_ -> will call getStaticProps on server -> render the page -> (background) Next.js will add it to path list and store in their local filesystem

```
export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [{ params: { id: "hello" } }, { params: { id: "world" } }],
    fallback: true,
  };
};
```

- If getStaticPath fallback = true, page will be served first, and getStaticPath later.
- So you should show a fallback loading page as well.

```
// @Dynamic Component
if (router.isFallback) {
  return <h1>Loading...</h1>;
}
```

###### getServerSideProps

- Similar to getStaticProps, but is ALWAYS called for EVERY PAGE REQUEST.
- Interesting for when data changes a lot (eg. real time dashboards)
- Try to _avoiding_ it: you loose a lot of optimization, cache, ...

```
export const getServerSideProps: GetServerSideProps = async (context) => { ... }
```
