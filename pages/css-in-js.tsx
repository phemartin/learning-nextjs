export function Heading() {
  return (
    <div>
      <h1>This is another heading!</h1>
      {/* CSS is universal w/ global tag */}
      <style jsx global>
        {`
          h1 {
            font-size: 1.5em;
          }
        `}
      </style>
    </div>
  );
}
export default function CssInJs() {
  // Custom variables
  const headingColor = "red";

  return (
    <div>
      <h1>Hello World!</h1>
      <Heading />
      {/* CSS is scoped to that particular component */}
      <style jsx>
        {`
          h1 {
            color: ${headingColor};
          }
        `}
      </style>
    </div>
  );
}
