// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { NextApiRequest, NextApiResponse } from "next";

// It's an API to the server
// http://localhost:3000/api/hello?test=333
export default function (req: NextApiRequest, res: NextApiResponse) {
  // if (req.method === 'POST') {
  //   res.json({message: "request is invalid"})
  // }
  console.log(req.query);

  res.statusCode = 200;
  res.setHeader("Content-Type", "application/json");
  res.json({ name: "John Doe" });
}
