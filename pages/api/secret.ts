import { NextApiRequest, NextApiResponse } from "next";
import jwt from "jsonwebtoken";

const KEY = "dh9832dhdh91823u";

export default (req: NextApiRequest, res: NextApiResponse) => {
  const { token } = req.body;
  const { admin } = jwt.verify(token, KEY) as { [key: string]: string };
  if (admin) {
    res.json({ secretCode: "123456" });
  } else {
    res.json({ admin: false });
  }
};
