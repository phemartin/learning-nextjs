import { NextApiRequest, NextApiResponse } from "next";
import jwt from "jsonwebtoken";

const KEY = "dh9832dhdh91823u";

// http://localhost:3000/api/auth
export default function (req: NextApiRequest, res: NextApiResponse) {
  if (req.method !== "POST" || !req.body) {
    res.statusCode = 404;
    res.end("Error");
    return;
  }

  const { username } = req.body;

  res.setHeader("Content-Type", "application/json");
  res.statusCode = 200;
  res.json({
    token: jwt.sign(
      {
        username,
        admin: username === "admin",
        hi: "there",
      },
      KEY
    ),
  });
}
