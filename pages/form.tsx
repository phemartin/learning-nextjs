import { useState } from "react";
import jwt from "jsonwebtoken";

export default function Form() {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const [message, setMessage] = useState<string>("Please login");
  const [secret, setSecret] = useState<string>("");

  async function submitForm(event) {
    event.preventDefault();
    const res = await fetch("/api/auth", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ username: username, password }),
    }).then((t) => t.json());

    const token = res.token;
    console.log(token);
    if (token) {
      const json = jwt.decode(token) as { [key: string]: string };
      console.log(json);
      setMessage(`Welcome ${json.username}!`);
      // is user admin?
      const res = await fetch("/api/secret", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ token }),
      }).then((t) => t.json());

      if (res.secretCode) {
        setSecret(res.secretCode);
      }
    } else {
      setMessage("Something went wrong!");
    }
  }
  return (
    <div>
      <form>
        <h1>{message}</h1>
        <input
          type="text"
          name="username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <br />
        <input
          type="password"
          name="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <br />
        <input type="submit" value="Login" onClick={submitForm} />
      </form>
      <h1>{secret}</h1>
    </div>
  );
}
