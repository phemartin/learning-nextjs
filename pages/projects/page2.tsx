import Link from "next/link";
import sassStyles from "../../styles/sass.module.scss";

export default function Page2() {
  return (
    <div>
      <h1>Page 2</h1>
      <p className={sassStyles.p}>
        Go back to <Link href="/">index page</Link>!
      </p>
    </div>
  );
}
