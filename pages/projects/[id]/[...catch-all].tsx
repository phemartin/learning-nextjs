import Link from "next/link";
import sassStyles from "../../styles/sass.module.scss";
import { useRouter } from "next/router";

// Catch all router after that
// http://localhost:3000/projects/:id/:1/:2/:3/:4/...

// If you want to include the base url(./index.tsx) -> name it: [[...catch-all.tsx]]
export default function ProjectId() {
  const router = useRouter();

  const takeHome = () => {
    router.push("/");
  };

  return (
    <div>
      <h1>Catch all route!</h1>
      <h1>Id: {router.query.id}</h1>
      <h1>Catch: {router.query["catch-all"]}</h1>
      <button onClick={takeHome}>Take me home</button>
    </div>
  );
}
