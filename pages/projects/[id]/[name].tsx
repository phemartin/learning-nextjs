import Link from "next/link";
import sassStyles from "../../../styles/sass.module.scss";
import { useRouter } from "next/router";

// http://localhost:3000/projects/:id/:name
export default function Projectname() {
  const router = useRouter();
  // console.log(router);

  const takeHome = () => {
    router.push("/");
    // router.replace("/"); // backbutton don't work
  };

  return (
    <div>
      <h1>Project id: {router.query.id}</h1>
      <h1>Project name: {router.query.name}</h1>
      <button onClick={takeHome}>Take me home</button>
    </div>
  );
}
