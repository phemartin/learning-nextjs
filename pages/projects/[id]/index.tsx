import Link from "next/link";
import sassStyles from "../../styles/sass.module.scss";
import { useRouter } from "next/router";

// http://localhost:3000/projects/:id
export default function ProjectId() {
  const router = useRouter();
  // console.log(router);

  const takeHome = () => {
    router.push("/");
  };

  return (
    <div>
      <h1>Project id: {router.query.id}</h1>
      <button onClick={takeHome}>Take me home</button>
    </div>
  );
}
