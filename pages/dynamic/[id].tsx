import { useRouter } from "next/router";
import { GetServerSideProps, GetStaticPaths, GetStaticProps } from "next";

// Execution on Server at build time, not run time
// Access all Nodejs stuff: database, filesystem, ...
export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context.params.id);
  return {
    // Map props to component
    props: {
      favoriteNumber: 4,
    },
    // Update getStaticProps every 10 seconds
    // Only works in production
    revalidate: 10,
  };
};

// Used when dynamic is also [variable], so we need to fetch the URL
// We use this to map path to which page that needs to getStaticProps

// BUILD START
// localhost:3000/dynamic/hello -> take the output -> store in the disk
// localhost:3000/dynamic/world -> take the output -> store in the disk

// If fallback = false -> Any other route will be discarted as 404
// If fallback = true -> It will accept any URL, but will only create static pages for the ones in the list
// eg. localhost:3000/dynamic/jewo73kqiejwo -> will call getStaticProps on server -> render the page ->
// -> (background) Next.js will add it to path list and store in their local filesystem
export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [{ params: { id: "hello" } }, { params: { id: "world" } }],
    fallback: true,
  };
};

// getServerSideProps -> Similar to getStaticProps, but is ALWAYS called. for EVERY PAGE REQUEST.
// Interesting for when data changes a lot (eg. real time dashboards)
// Try to avoid it: you loose a lot of optimization, cache, ...
// export const getServerSideProps: GetServerSideProps = async (context) => {}

export default function Dynamic(props) {
  const router = useRouter();

  // If getStaticPath fallback = true, page will be served first, and getStaticPath later
  // So you should show a fallback loading page as well
  // Test here: http://localhost:3000/dynamic/123
  if (router.isFallback) {
    return <h1>Loading...</h1>;
  }

  return <h1>My favorite number is: {props.favoriteNumber}</h1>;
}
