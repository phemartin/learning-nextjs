import "../styles/global.css";

export default function App({ Component, pageProps }) {
  // Executed in both server and client
  console.log("Hello from _app;");
  return <Component {...pageProps} />;
}
