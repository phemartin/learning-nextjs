import Head from "next/head";
import styles from "../styles/index.module.css";
import sassStyles from "../styles/sass.module.scss";
import Link from "next/link";

export default function Home() {
  return (
    <div>
      <Head>
        <title>Learning Next.js</title>
      </Head>
      <h1 className={styles.title}>
        <a href="https://nextjs.org/">Hello World!</a>
      </h1>
      <p className={sassStyles.purple}>
        This is purple and <span>underlined</span>!
      </p>
      <p className={sassStyles.p}>
        Go to <Link href="/projects/page2">page 2</Link>!
      </p>
    </div>
  );
}
